import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";


// reuseable component currently used for the circle in favorites screen
export default class companySize extends Component {
    render() {
        return (
            <View style={styles.container}>
                <MaterialCommunityIconsIcon
                    name="account"
                    style={styles.iconStyle}
                />
                <Text style = {styles.companySizeText}>100-200</Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
    },
    iconStyle: {
        fontSize: 50,
        color: "black",
        opacity: 1.0,
    },
    companySizeText: {
        fontSize: 16,
    }
})