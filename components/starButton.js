import React, { memo } from "react";
import {TouchableOpacity } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/FontAwesome";

const starButton = ({onStarPress, outerComponentStyle, innerComponentStyle}) => (
    <TouchableOpacity onPress = {onStarPress} style = {outerComponentStyle}>
          <MaterialCommunityIconsIcon
            name="star"
            style={innerComponentStyle}
          ></MaterialCommunityIconsIcon>
    </TouchableOpacity>
);

export default memo(starButton);