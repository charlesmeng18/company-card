import React, { memo } from "react";
import {TouchableOpacity } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/FontAwesome";

const bookMarkButton = ({onbookMarkPress, outerComponentStyle, innerComponentStyle}) => (
    <TouchableOpacity onPress = {onbookMarkPress} style = {outerComponentStyle}>
          <MaterialCommunityIconsIcon
            name="bookmark"
            style={innerComponentStyle}
          ></MaterialCommunityIconsIcon>
    </TouchableOpacity>
);

export default memo(bookMarkButton);