import React, {Component} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";


// reuseable component currently used for the circle in favorites screen
export default class companySize extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style = {styles.trashIconContainer}>
                    <Icon
                        name = "trash"
                        style = {styles.trashIcon}
                    />
                </View>
                <View style = {styles.chatIconContianer}>
                    <MaterialCommunityIconsIcon
                        name="forum"
                        style={styles.chatIcon}
                    ></MaterialCommunityIconsIcon>
                </View>
                
                <View style = {styles.checkboxContainer}>
                    <Image
                        style = {styles.checkbox}
                        source = {require("../assets/checklist.jpg")}
                    />
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
    },
    trashIconContainer: {
        flex: 0.33,
        justifyContent: "center",
        alignItems: "center",
    },
    trashIcon: {
        fontSize: 40,
    },  
    chatIconContianer: {
        flex: 0.34,
        justifyContent: "center",
        alignItems: "center",
    },
    chatIcon: {
        fontSize: 40,
        marginTop: 5,
    },
    checkboxContainer: {
        flex: 0.33,
        justifyContent: "center",
        alignItems: "center",
    },
    checkbox: {
        width: 60,
        height: 30,
    },
})