import React, { memo } from "react";
import {TouchableOpacity, StyleSheet, Image } from "react-native";


const seedStagesButton = ({onseedStagesPress}) => (
    <TouchableOpacity onPress = {onseedStagesPress}>
          <Image style={styles.image} source={require("../assets/seedSmall.png")} />
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    image: {
        width: 30,
        height: 30,
        resizeMode: "cover",
    }
})

export default memo(seedStagesButton);