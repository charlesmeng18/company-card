import React, { memo } from "react";
import {TouchableOpacity } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/FontAwesome";

const chatButton = ({onChatPress, outerComponentStyle, innerComponentStyle}) => (
    <TouchableOpacity onPress = {onChatPress} style = {outerComponentStyle}>
          <MaterialCommunityIconsIcon
            name="comments"
            style={innerComponentStyle}
          ></MaterialCommunityIconsIcon>
    </TouchableOpacity>
);

export default memo(chatButton);