import React, {Component} from 'react';
import {Image, StyleSheet, View, TouchableOpacity } from 'react-native';

export default class navBar extends Component {

    render() {
        return (
            // default navigation bar
            <View style = {styles.navBarContainer}>
                {/* account button redirects to profile */}
                <View style = {styles.accountContainer}>
                    <TouchableOpacity>
                        <Image
                            source = {require("../assets/account.png")}
                            style = {styles.account}
                        />
                    </TouchableOpacity>
                </View>

                {/* navigator button redirects to... */}
                <View style = {styles.navigatorContainer}>
                    <TouchableOpacity>
                        <Image
                            source = {require("../assets/compass.png")}
                            style = {styles.compass}
                        />
                    </TouchableOpacity>
                </View>

                {/* seedstages button redirects to home */}
                <View style = {styles.seedStagesContainer}>
                    <TouchableOpacity>
                        <Image
                            source = {require("../assets/seedSmall.png")}
                            style = {styles.seedStages}
                        />
                    </TouchableOpacity>
                </View>                

                {/* chat button redirects to messages */}
                <View style = {styles.chatContainer}>
                    <TouchableOpacity>
                        <Image
                            source = {require("../assets/chat.png")}
                            style = {styles.chat}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    navBarContainer: {
        flexDirection: "row",
    },
    accountContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 0.25,
    },
    account: {
        width: 55,
        height: 55,
    },
    navigatorContainer: {
        justifyContent: "center",
        alignItems: "flex-end",
        flex: 0.25,
    },
    compass: {
        width: 55,
        height: 55,
    },
    seedStagesContainer: {
        justifyContent: "center",
        alignItems: "flex-start",
        flex: 0.25,
    }, 
    seedStages: {
        width: 55,
        height: 55,
    },
    chatContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 0.25,
    },
    chat: {
        width: 55,
        height: 55,
    },
})