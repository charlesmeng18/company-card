import React, {Component} from 'react';
import {StyleSheet, Text, Image, View} from 'react-native';


// reuseable component for development stage graph
export default class developmentStage extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style = {styles.salaryText}>$5m</Text>
                <Image
                    source = {require("../assets/stage.jpg")}
                    style = {styles.graph}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
       // edit container style if needed 
    },
    salaryText: {
        fontSize: 18,
    },
    graph: {
        height: 60,
        width: 60, 
    },
})