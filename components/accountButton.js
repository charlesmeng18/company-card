import React, { memo } from "react";
import {TouchableOpacity } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";

const accountButton = ({onAccountPress, outerComponentStyle, innerComponentStyle}) => (
    <TouchableOpacity onPress = {onAccountPress} style = {outerComponentStyle}>
          <MaterialCommunityIconsIcon
            name="account"
            style={innerComponentStyle}
          ></MaterialCommunityIconsIcon>
    </TouchableOpacity>
);

export default memo(accountButton);