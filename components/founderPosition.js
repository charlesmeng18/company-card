import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';


// reuseable component currently used for the circle in favorites screen
export default class companySize extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style = {styles.foundedText}>1997 {"\n"}</Text>
                <Text style = {styles.positionsAvailText}>4 openings</Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
    },
    foundedText: {
        fontSize: 16,
        color: "black",
    },
    positionsAvailText: {
        fontSize: 16,
        color: "#32CD32",
    }
})