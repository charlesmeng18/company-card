import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';


// reuseable component currently used for the circle in favorites screen
export default class circle extends Component {
    render() {
        return (
            <View style={styles.circle}>
                <Text> 85 </Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    circle: {
        width: 50,
        height: 50,
        borderRadius: 50/2,
        borderColor: "#32CD32",
        borderWidth: 3,
        justifyContent: "center",
        alignItems: "center",
    },
})
