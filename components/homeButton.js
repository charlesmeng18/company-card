import React, { memo } from "react";
import {TouchableOpacity } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";

const homeButton = ({onHomePress, outerComponentStyle, innerComponentStyle}) => (
    <TouchableOpacity onPress = {onHomePress} style = {outerComponentStyle}>
          <MaterialCommunityIconsIcon
            name="home"
            style={innerComponentStyle}
          ></MaterialCommunityIconsIcon>
    </TouchableOpacity>
);

export default memo(homeButton);