import React, { memo } from "react";
import {TouchableOpacity } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/FontAwesome";

const alertButton = ({onAlertPress, outerComponentStyle, innerComponentStyle}) => (
    <TouchableOpacity onPress = {onAlertPress} style = {outerComponentStyle}>
          <MaterialCommunityIconsIcon
            name="bell"
            style={innerComponentStyle}
          ></MaterialCommunityIconsIcon>
    </TouchableOpacity>
);

export default memo(alertButton);