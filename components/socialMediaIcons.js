import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import { SocialIcon } from 'react-native-elements'
import MaterialIcon from "react-native-vector-icons/MaterialCommunityIcons";

// reuseable component currently used for the circle in favorites screen
export default class Icons extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style = {styles.faceContainer}>
                    <SocialIcon
                        type='facebook'
                    />
                </View>
                
                <View style = {styles.twitContainer}>
                    <SocialIcon
                        type='instagram'
                    />
                </View>
                
                <View style = {styles.instaContainer}>
                    <SocialIcon
                        type='twitter'
                    />
                </View>
                
                <View style = {styles.webContainer}>
                    <MaterialIcon
                        name ='web'
                        style = {styles.webIcon}
                    />
                </View>
                
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    faceContainer: {
        alignItems: "center",
        justifyContent: "center",
    },
    twitContainer: {
        alignItems: "center",
        justifyContent: "center",
    },
    instaContainer: {
        alignItems: "center",
        justifyContent: "center",
    },
    webContainer: {
        alignItems: "center",
        justifyContent: "center",
    },
    webIcon: {
        fontSize: 60,
    }
})