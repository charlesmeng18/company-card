import React, { memo } from "react";
import {TouchableOpacity } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";

const listButton = ({onlistPress, outerComponentStyle, innerComponentStyle}) => (
    <TouchableOpacity onPress = {onlistPress} style = {outerComponentStyle}>
          <MaterialCommunityIconsIcon
            name="view-list"
            style={innerComponentStyle}
          ></MaterialCommunityIconsIcon>
    </TouchableOpacity>
);

export default memo(listButton);