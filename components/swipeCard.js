import React, {Component} from 'react';
import {Image, StyleSheet, Text, TextInput, View, TouchableOpacity } from 'react-native';
import {Card} from 'react-native-elements';
import Circle from "./circle";
import Dstage from "./developmentStage";
import Csize from "./companySize";
import Misc from "./founderPosition";
import IconBar from "./socialMediaIcons";
import {genericText} from "../constants/genericText";



// reuseable component currently used for favorites screen and swipe screen
export default class swipeCard extends Component {
    constructor(props) {
        super(props)
        this.state = {descriptionLines: 5}
        this.changeState = this.changeState.bind(this)
    }

    changeState() {
        if (this.state.descriptionLines == 5) { 
            this.setState({
                descriptionLines: 20,
            })
        } else {
            this.setState({
                descriptionLines: 5,
            })
        }
    }

    render() {
        return (
            // on click the card should expand
            <TouchableOpacity onPress = {this.changeState} >
                {/* favorites/swipe screen card*/}
                <Card containerStyle = {styles.cardContainer}>
                    {/* View that contains the company logo, title, location, and circle*/}
                    <View style = {styles.topFourth}>
                        {/* image for company logo*/}
                        <Image
                            style = {styles.logo}
                            source = {{
                                uri: "http://public.crunchbase.com/t_api_images/v1448788261/bfpkkq6vlyedrctecfv4.png"
                                // need to replace uri with company 
                            }}
                        />
                        {/* View that contains company title/location*/}
                        <View style = {styles.titleContainer}>
                            <Text style = {styles.titleText} numberOfLines = {2}> The Rowdy </Text>
                            <Text style = {styles.locationText}> Los Angeles </Text>
                        </View>
                        {/* View that contains circle */}
                        <View style = {styles.circleContainer}>
                            <Circle/>
                        </View>
                    </View>

                    {/* View that contains startup development stage, size of company and openings*/}
                    <View style = {styles.middleFourth}>
                        <View style = {styles.DstageContainer}>
                            <Dstage/>
                        </View>
                        <View style = {styles.CsizeContainer}>
                            <Csize/>
                        </View>
                        <View style = {styles.MiscContainer}>
                            <Misc/>
                        </View>
                    </View>

                    {/* View that contains icons*/}
                    <View style = {styles.middleFourth2}>
                        <IconBar/>
                    </View>

                    {/* View that contains description*/}
                    <View style = {styles.bottomFourth}>
                            <Text numberOfLines = {this.state.descriptionLines}>{genericText} </Text>
                    </View>
                </Card>
            </TouchableOpacity>
        )
    }
     
}


const styles = StyleSheet.create({
    cardContainer: {
        borderRadius: 15,
        backgroundColor: "white",
        borderColor: "red",
        borderWidth: 2,
    },
    topFourth: {
        flexDirection: "row",
    },
    logo: {
        width: 120,
        height: 80,
        borderRadius: 50,
        resizeMode: "center",
    },
    titleContainer: {
        flex: 0.7,
        justifyContent: "center",
        alignItems: "center",
    }, 
    titleText: {
        fontSize: 20,
        color: "gray",
    },
    locationText: {
        fontSize: 16,
        color: "gray",
    },
    circleContainer: {
        flex: 0.3,
        justifyContent: "center",
        alignItems: "center",
    },
    middleFourth: {
        flexDirection: "row",
    },
    DstageContainer: {
        flex: 0.33,
        justifyContent: "center",
        alignItems: "center",
    },
    CsizeContainer: {
        flex: 0.34,
        justifyContent: "center",
        alignItems: "center",
    },
    MiscContainer: {
        flex: 0.33,
        justifyContent: "center",
        alignItems: "center",
    },
    middleFourth2: {
        justifyContent: "center",
        alignItems: "center",
    },
    bottomFourth: {
        fontSize: 14,
    },
})
