import {StyleSheet} from "react-native"
import Constants from 'expo-constants';
import {screenWidth, screenHeight} from './constants/dimensions';

export default StyleSheet.create({
    // imported stylesheet
    container: {
        flex: 1,
        marginTop: Constants.statusBarHeight,
      },
    item: {
      backgroundColor: 'white',
      padding: 15,
      marginVertical: 8,
      marginHorizontal: 16,
      borderRadius: 25,
      borderWidth: 2,
      borderColor: "red",
      shadowColor: 'grey',
      shadowOpacity: 1.0,
    },
    box: {
      flexDirection: "row",
      flexWrap: "wrap",
      // backgroundColor: "black",
    },
    logoContainer: {
      // backgroundColor: "yellow",
      width: 111,
      height: 81,
    },
    overviewContainer: {
      marginVertical: 8,
      //backgroundColor: "blue",
      width: 119,
      height: 81,
    },
    visualsContainer: {
      // backgroundColor: "purple",
      width: 77,
      height: 150,
      marginLeft: 2,
    },
    detialsContainer: {
      // backgroundColor: "green",
      position: 'absolute',
      width: 230,
      marginTop: 87,
      height: 69
    },
    companyLogo: {
      width: 110,
      height: 80,
      borderRadius: 100,
      resizeMode: "cover",
    },
    title: {
      fontSize: 20,
    },
    location: {
      fontSize: 13,
    },
    position: {
      fontSize: 13,
    },
    description: {
      fontSize: 14,
    },
    funding: {
      marginVertical: 8,
      fontSize: 22,
      marginBottom: 10,
    },
    graph: {
      width: 70,
      height: 40,
      marginBottom: 6,
    },
    companySizeContainer:{
      flexDirection: "row",
    },
    accountIcon: {
      fontSize: 20,
    },
    employeeCount: {
      fontSize: 16,
    },
    salary: {
      fontSize: 22,
    },
    buttonGroup: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 8,
      borderTopWidth: 5,
      borderTopColor: 'black',
    },
    homeContainer: {
      width: screenWidth/5,
      alignItems: 'center',
    },
    chatContainer:{
      width: screenWidth/5,
      alignItems: 'center',
    },
    bookMarkContainer: {
      width: screenWidth/5,
      alignItems: 'center',
    },
    alertContainer: {
      width: screenWidth/5,
      alignItems: 'center',
    },
    accountContainer: {
      width: screenWidth/5,
      alignItems: 'center',
    },
    icon1: {
      fontSize: 30,
      color: "#000",
      opacity: 0.5,
    },
    icon2: {
      fontSize: 30,
      color: "#000",
      opacity: 0.5,
    },
    icon3: {
      fontSize: 30,
      color: "#000",
      opacity: 0.5,
    },
    icon4: {
      fontSize: 30,
      color: "#000",
      opacity: 0.5,
    },
    icon5: {
      fontSize: 30,
      color: "#000",
      opacity: 0.5,
    },

});