import React from 'react';
import { FlatList, Text, View, ScrollView, StyleSheet} from 'react-native';
import company from './data/neji.json';
import {Card} from 'react-native-elements';
import Constants from 'expo-constants';

const imageURL = company["item"]["image_asset_path"]
const websiteURL = company["websites"]["homepage"]

export default function CompanyDescription() {
    return (
        <ScrollView style={styles.container}>
            <Card
                title= {company["name"]}
                titleStyle= {{fontSize:"24px"}}
                image={{uri: imageURL}}>
            </Card>
            <View style={styles.item}>
                <Text>{websiteURL}</Text>
            </View>
            <View style={styles.item}>
                <Text style={styles.description}>{company["short_description"]}</Text>
            </View>
            <View style={styles.item}>
                <Text> Range of Employees is: {company["num_employees_min"]} to {company["num_employees_max"]}</Text>
            </View>
            <View style={styles.item}>
                <Text style={styles.descriptionTitle}> Funding Information</Text>
                <Text> Total Funding: {company["total_funding_usd"]}</Text>
                    <View style={styles.description}>
                        <Text>Round 1: {company["item"]["funding_rounds"][0]["funding_type"]}, {company["item"]["funding_rounds"][0]["announced_on"]}, ${company["item"]["funding_rounds"][0]["money_raised_usd"]} </Text>
                        <Text>Round 1: {company["item"]["funding_rounds"][1]["funding_type"]}, {company["item"]["funding_rounds"][1]["announced_on"]}, ${company["item"]["funding_rounds"][1]["money_raised_usd"]} </Text>
                    </View>
            </View>

            <View style={styles.item}>
                    <Text style={styles.description}>
                        Visit Us: {websiteURL}
                    </Text>

            </View>

        </ScrollView>
    )
};
const styles = StyleSheet.create({
    title: {
        fontSize: 38,
        marginHorizontal: 16,
    },
    descriptionTitle:{
        fontSize: 24,
        fontWeight: "bold",
        marginBottom: 10,
    },
    container: {
        flex: 1,
        marginTop: Constants.statusBarHeight,
        backgroundColor: 'lightblue',
    },
    item: {
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 10,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },

    description: {
        fontSize: 14,
        textAlign: 'center',
    }
    });
