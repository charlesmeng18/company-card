import { Platform } from 'react-native';

const API = 'http://localhost:3001/';
// const API = 'http://192.168.1.91:3001/'; // TODO: change this IP address to your own internal IP address

export const apiMiddleware = store => next => action => {
  // Pass all actions through by default
  next(action);
  switch (action.type) {
    // In case we receive an action to send an API request
    case 'GET_COMPANIES_DATA':
      // Dispatch GET_COMPANIES_DATA_LOADING to update loading state
      store.dispatch({type: 'GET_COMPANIES_DATA_LOADING'});
      // Make API call and dispatch appropriate actions when done
      fetch(API + 'companies')
        .then(
          response => response.json()
        )
        .then(data => next({
          type: 'GET_COMPANIES_DATA_RECEIVED',
          data
        }))
        .catch(error => next({
          type: 'GET_COMPANIES_DATA_ERROR',
          error
        }));
      break;
    case 'GET_JOBS_DATA':
    // Dispatch GET_COMPANIES_DATA_LOADING to update loading state
    store.dispatch({type: 'GET_COMPANIES_DATA_LOADING'});
    // Make API call and dispatch appropriate actions when done
    fetch(API + 'jobs')
      .then(
        response => response.json()
      )
      .then(data => next({
        type: 'GET_JOBS_DATA_RECEIVED',
        data
      }))
      .catch(error => next({
        type: 'GET_JOBS_DATA_ERROR',
        error
      }));
      break;
    // Do nothing if the action does not interest us
    default:
      break;
  }
};

export const reducer = (state = { companies: [], loading: true }, action) => {
  switch (action.type) {
    case 'GET_COMPANIES_DATA_LOADING':
      return {
        ...state,                   // keep the existing state,
        loading: true,              // but change loading to true
      };
    case 'GET_COMPANIES_DATA_RECEIVED':
      //console.log(action.data);
      return {
        loading: false,             // set loading to false
        companies: action.data, // update companies array with reponse data
      };
    case 'GET_COMPANIES_DATA_ERROR':
      return state;
    default:
      return state;
    }
};
