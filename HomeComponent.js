import React, { Component } from 'react';
import { ScrollView, View, Text } from 'react-native';
import { Card } from 'react-native-elements';


class Home extends Component {

  render() {
    return(
      <View>
        <Card title="Home">
          <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ac aliquet ex, eget tincidunt risus. Mauris a est sed lacus consequat ornare pulvinar at nulla. Maecenas a lacinia elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent convallis ornare massa, vitae pharetra elit vestibulum sit amet. Sed sed condimentum ex. Proin ac urna nec magna hendrerit fringilla ut at mauris. Cras nibh arcu, dictum eget gravida ut, imperdiet et dolor. Nunc neque nibh, egestas et ante id, auctor ornare erat. </Text>
        </Card>
      </View>
    );

  }
}


export default Home;
