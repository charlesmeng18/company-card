import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ListScreen from './ListScreen';
import DetailsScreen from './DetailsScreen';
import Jobs from './JobsComponent';
import SwipeScreen from './Swiper/SwipeScreen'
import FavoritesScreen from './FavoritesScreen'
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { apiMiddleware, reducer } from './redux/redux';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import DetailsScreenNavigator from './DetailsScreenNavigator'
import MessagesMain from './MessagesMain'

console.disableYellowBox = true; // Disable warning messages

// Create Redux store
const store = createStore(reducer, {}, applyMiddleware(apiMiddleware));
// Fetch companies data
store.dispatch({type: 'GET_COMPANIES_DATA'});

// Initialize Bottom Tab Navigator
const Tab = createBottomTabNavigator();


// Central Tab Navigator that links to Various Screens
function MyTabs() {
  return (
    <Tab.Navigator>
      {/* Nested Within DetailsScreenNavigator is Stack Navigator from List to DetailsScreen */}
      <Tab.Screen name="DetailsScreenNavigator" component={DetailsScreenNavigator} options={{title:"List of Companies"}}/>
      <Tab.Screen name="Swipe" component={SwipeScreen} options={{title:"Matchmaking"}}/>
      <Tab.Screen name="Favorites" component={FavoritesScreen} options={{title:"Favorited Companies"}}/>
      <Tab.Screen name="Messages" component={MessagesMain} options={{title: "Messages"}}/>
    </Tab.Navigator>
  )
}

export default class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <MyTabs/>
        </NavigationContainer>
      </Provider>
    );
  }
}
