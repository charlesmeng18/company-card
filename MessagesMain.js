// Import the screens
import React from 'react-native'
import {Text, View} from 'react-native'
import MessagesScreen from './MessagesScreen';
import Chat from './Chat';
// Import React Navigation
import { createStackNavigator } from '@react-navigation/stack'

// Create the navigator
const navigator = createStackNavigator({
  MessagesScreen: { screen: MessagesScreen },
  Chat: { screen: Chat },
});

const Stack = createStackNavigator();

export default function MessagesMain() {
    return (
        // <Stack.Navigator>
        //     <Stack.Screen name="MessagesScreen" component={MessagesScreen}/>
        //     <Stack.Screen name="Chat" component={Chat}/>
        // </Stack.Navigator>
        {navigator}
    )
}