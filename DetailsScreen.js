import React, {Component} from 'react'
import {StyleSheet, ScrollView, View, Text, FlatList, Image, Button } from 'react-native'
import {Card} from 'react-native-elements';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {LinearGradient} from 'expo-linear-gradient';
import {screenWidth} from "./constants/dimensions";
import Seed from "./components/seedstagesButton";
import Chat from "./components/chatButton";
import List from "./components/listButton"
import Star from "./components/starButton";
import Loading from './LoadingComponent'
import { TouchableOpacity } from 'react-native-gesture-handler';

// const JOBS = 'http://localhost:3001/jobs';
// const JOBS = 'http://192.168.1.91:3001/jobs';

export default function DetailsScreen({route}) {
    const {title, headquarters_city, 
      headquarters_country, image_url, 
      description, url, min_employees, 
      max_employees, total_funding_usd, 
      founders_array} 
    = route.params;

    const min = min_employees["min_employees"]
    const max = max_employees["max_employees"]
    let image = image_url["image_url"]

    // Tab Navigator
    const Tab = createMaterialTopTabNavigator();
    function MyTabs() {
      return (
        <NavigationContainer independent={true}>
          <Tab.Navigator>
            {/* List out the different tabs that we want on the company details page */}
            <Tab.Screen name="About" component={AboutScreen}/>
            <Tab.Screen name="Job Roles" component={JobsScreen}/>
            <Tab.Screen name="Contacts" component={ContactsScreen}/>
          </Tab.Navigator>
        </NavigationContainer>
      )
    }

    // Tab of ContactsScreen (swipe left), nested within the Details Screen
    function ContactsScreen() {
      return (
        <FlatList
          data={founders_array["founders_array"]}
          renderItem={({item}) => (
            <Card title={`${item.properties.first_name}` + " " + `${item.properties.last_name}`}>
              <Text>{item.properties.bio}</Text>
            </Card>
            )
          }
        />
      )
    }

    
    // Tab of JobsScreen (swipe left), nested within the Details Screen
    function JobsScreen() {
      return (
        <ScrollView style={styles.container}>
          <View style = {styles.jobContainer}>
            <View style = {styles.topContainer}>

              <View style = {styles.seedStagesContainer}>
                <Seed 
                  // onStarPress = {() => navigation.navigate("Details")}
                />
              </View>

              <View style = {styles.chatContainer}>
                <Chat 
                  // onStarPress = {() => navigation.navigate("Details")}
                  // outerComponentStyle = {styles.leftBtn2} 
                  innerComponentStyle = {styles.icon2} />
              </View>

              <View style = {styles.checkContainer}>
                <List
                  // onStarPress = {() => navigation.navigate("Details")}
                  // outerComponentStyle = {styles.rightBtn1} 
                  innerComponentStyle = {styles.icon3} />
              </View>

              <View style = {styles.starContainer}>
                <Star 
                  // onStarPress = {() => navigation.navigate("Details")}
                  // outerComponentStyle = {styles.rightBtn2} 
                  innerComponentStyle = {styles.icon4} />
              </View>
            </View>
            {/* <FlatList data={jobs}
              renderItem={({item}) => (
                <Card title={`${item.properties.salary}`}>
                  <Text>{item.properties.bio}</Text>
                </Card>
                )
              }
            /> */}
            <JobCard url = {image} 
                title={title["title"]} 
                headquarters_city={headquarters_city["headquarters_city"]} 
                headquarters_country={headquarters_country["headquarters_country"]}
                description = {description["description"]}
                min={min}
                max={max}
                total_funding_usd={fnum(total_funding_usd["total_funding_usd"])}
              />
          </View> 
        </ScrollView>       
      )
    }

    // Tab of AboutScreen, nested within the Details Screen
    function AboutScreen() {
      return (
        <ScrollView style={styles.container}>
          <LinearGradient colors={['lightblue', 'blue', 'teal', 'teal', 'lightblue', 'lightblue']}>
          <DetailsTitleCard url = {image} 
            title={title["title"]} 
            headquarters_city={headquarters_city["headquarters_city"]} 
            headquarters_country={headquarters_country["headquarters_country"]}
            min={min}
            max={max}
            total_funding_usd={fnum(total_funding_usd["total_funding_usd"])}
          />

          <Card containerStyle={styles.cardContainer} title={"About"}>
            <Text style={styles.description}>{description["description"]}</Text>
          </Card>

          <Card>
              <Text>
                Product details, culture
              </Text>
          </Card>
          <Card>
              <Text>
                Other Stuff for Startup to Showcase
              </Text>
          </Card>
          </LinearGradient>
        </ScrollView>
      )
    }

    return (
      <MyTabs/>
    )
  };

// Header Card on the Details Page, Does not Display Image if url is null
function DetailsTitleCard(props) {
  if (props.url != null) {
    // Card that is returned if Image URL is not null
    return (
      <Card 
        // title={props.title} titleStyle={styles.title}
        containerStyle={styles.cardContainer}
      >
        <View style={styles.titleContainer}>
          <Image style={styles.detailsImage} source={{uri: props.url}} resizeMode="center"></Image>
          <Text style={styles.title}>{props.title}</Text>
        </View>
        <View>          
          <Text style={styles.description}>HQ: {props.headquarters_city}, {props.headquarters_country}</Text>
        </View>

        <View style={{flexDirection: 'row'}}>
          <EmployeesDetail min={props.min} max={props.max}/>
          <FundingDetail value={props.total_funding_usd}/>
        </View>
      </Card>
      )
      // Card that is returned if Image URL is null
  } else {
    return (
      <Card 
        containerStyle={styles.cardContainer}
      >
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{props.title}</Text>
        </View>
        <View>          
          <Text style={styles.description}>HQ: {props.headquarters_city}, {props.headquarters_country}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <EmployeesDetail min={props.min} max={props.max}/>
          <FundingDetail value={props.total_funding_usd}/>
        </View>
      </Card>
    )
  }
}

// card creation for jobs screen
function JobCard(props) {
  return(
  <Card containerStyle={styles.jobCardContainer}>
        <View style={styles.titleContainer}>
          <Image style={styles.detailsImage} source={{uri: props.url}} resizeMode="center"></Image>
          <View>
            <Text style={styles.title}>{props.title}</Text>
            <Text style = {{fontSize: 16, padding: 10,}}>{props.headquarters_city}</Text>
          </View>
        </View>
        {/* <View>          
          <Text style={styles.description}> {props.headquarters_city}, {props.headquarters_country}</Text>
        </View> */}

        <View style = {{alignItems: "center", justifyContent: "center", marginBottom: 4, marginTop: 2,}}>
          <Text style = {{fontSize: 18, color: "blue", }}> Sprint       Remote        $10/h</Text>
        </View>

        <View style = {{marginTop: 2}}>
          <Text>{props.description} </Text>
        </View>

        <View style = {{alignContent: "center", justifyContent: "center", flexDirection: "row", marginTop: 3,}}>
          <View style = {{flex: 0.33, alignItems: 'center',}}>
            <Chat innerComponentStyle = {styles.icon2} />
          </View>
          <View style = {{flex: 0.34, alignItems: 'center',}}>
            <Star innerComponentStyle = {styles.icon4}/>
          </View>
          <View style = {{flex: 0.33, alignItems: 'center',}}>
            <Button color = "gray" title = "Apply"/>
          </View>
        </View>
      </Card>
  )
}
 


// min and max number of employees, do not display if null
function EmployeesDetail(props) {
  if (props.min != null && props.max != null) {
    return (
      <Text style={{flex:1, justifyContent: 'center'}}> Employees: {props.min}-{props.max}</Text>
    )
  } return (
    <Text style={{flex:1, justifyContent: 'center'}}> Employees: NA </Text>
  )
};

// Do not display numbers or data on details screen if null (General Component)
function Detail(props) {
  if (props.value != null) {
    return (
      <Text>{props.value} </Text>
    )
  } return null;
};

// Do not display funding numbers if null
function FundingDetail(props) {
  if (props.value != null) {
    return (
      <Text style={{flex:1, justifyContent: 'center'}}>Total Funding: ${props.value} </Text>
    )
  } else{
    return (
      <Text style={{flex:1}}>Total Funding: NA </Text>
    )
  };
};

function fnum(x) {
  if(isNaN(x)) return x;

  if(x < 9999) {
      return x;
  }
  if(x < 1000000) {
      return Math.round(x/1000) + "K";
  }
  if( x < 10000000) {
      return (x/1000000).toFixed(2) + "M";
  }
  if(x < 1000000000) {
      return Math.round((x/1000000)) + "M";
  }
  if(x < 1000000000000) {
      return Math.round((x/1000000000)) + "B";
  }
  return "1T+";
};

const styles = StyleSheet.create({
  title: {
    fontSize: 32,
    borderRadius: 40,
    flex: 1,
    padding: 10,
  },
  titleContainer: {
    flexDirection: 'row', 
    flex: 2,
  },
  container: {
    flex: 1,
    backgroundColor: 'lightblue',
  },
  descriptionTitle:{
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 10,
  },
  description: {
    fontSize: 16,
    margin: 5,
    textAlign: 'center',
  },
  detailsImage: {
    flexDirection: 'row', 
    flex: 1,
    padding: 10,
    borderRadius: 10000,
  },
  cardContainer: {
    borderRadius: 15,
    backgroundColor: "white",
  },
  jobContainer: {

  },
  topContainer: {
    top: 0,
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 8,
  },
  seedStagesContainer: {
    width: screenWidth/4,
    alignItems: 'center',
  },
  chatContainer: {
    width: screenWidth/4,
    alignItems: 'center',
  },
  checkContainer: {
    width: screenWidth/4,
    alignItems: 'center',
  },
  starContainer: {
    width: screenWidth/4,
    alignItems: 'center',
  },
  icon1: {
    fontSize: 30,
    color: "#000",
    opacity: 0.5,
  },
  icon2: {
    fontSize: 30,
    color: "#000",
    opacity: 0.5,
  },
  icon3: {
    fontSize: 30,
    color: "#000",
    opacity: 0.5,
  },
  icon4: {
    fontSize: 30,
    color: "gold",
    // opacity: 100,
  },
  jobCardContainer: {
    borderWidth: 2,
    borderColor: "gray",
    borderRadius: 15,
  }
})

// export default class DetailsScreen extends Component {
//   constructor (props) {
//       super(props);

//       this.state={
//           jobs:[]
//       }
//   }

//   // Grab jobs from Server, set to State
//   jobDidMount = () => {
//     fetch(JOBS, {
//         method: 'GET'
//     })
//     .then((response) => response.json())
//     .then((responseJson) => {
//         console.log(responseJson);
//         this.setState({
//           jobs: responseJson
//         })
//     })
//   }

//   render() {
//     Details({route});
//   }  
// }

// function Details({route}) {
//     const {title, headquarters_city, 
//       headquarters_country, image_url, 
//       description, url, min_employees, 
//       max_employees, total_funding_usd, 
//       founders_array} 
//     = route.params;

//     const min = min_employees["min_employees"]
//     const max = max_employees["max_employees"]
//     let image = image_url["image_url"]

//     // Tab Navigator
//     const Tab = createMaterialTopTabNavigator();
//     function MyTabs() {
//       return (
//         <NavigationContainer independent={true}>
//           <Tab.Navigator>
//             {/* List out the different tabs that we want on the company details page */}
//             <Tab.Screen name="About" component={AboutScreen}/>
//             <Tab.Screen name="Job Roles" component={JobsScreen}/>
//             <Tab.Screen name="Contacts" component={ContactsScreen}/>
//           </Tab.Navigator>
//         </NavigationContainer>
//       )
//     }

//     // Tab of ContactsScreen (swipe left), nested within the Details Screen
//     function ContactsScreen() {
//       return (
//         <FlatList
//           data={founders_array["founders_array"]}
//           renderItem={({item}) => (
//             <Card title={`${item.properties.first_name}` + " " + `${item.properties.last_name}`}>
//               <Text>{item.properties.bio}</Text>
//             </Card>
//             )
//           }
//         />
//       )
//     }

    
//     // Tab of JobsScreen (swipe left), nested within the Details Screen
//     function JobsScreen() {
//       return (
//         <ScrollView style={styles.container}>
//           <View style = {styles.jobContainer}>
//             <View style = {styles.topContainer}>

//               <View style = {styles.seedStagesContainer}>
//                 <Seed 
//                   // onStarPress = {() => navigation.navigate("Details")}
//                 />
//               </View>

//               <View style = {styles.chatContainer}>
//                 <Chat 
//                   // onStarPress = {() => navigation.navigate("Details")}
//                   // outerComponentStyle = {styles.leftBtn2} 
//                   innerComponentStyle = {styles.icon2} />
//               </View>

//               <View style = {styles.checkContainer}>
//                 <List
//                   // onStarPress = {() => navigation.navigate("Details")}
//                   // outerComponentStyle = {styles.rightBtn1} 
//                   innerComponentStyle = {styles.icon3} />
//               </View>

//               <View style = {styles.starContainer}>
//                 <Star 
//                   // onStarPress = {() => navigation.navigate("Details")}
//                   // outerComponentStyle = {styles.rightBtn2} 
//                   innerComponentStyle = {styles.icon4} />
//               </View>
//             </View>
//             {/* <FlatList data={jobs}
//               renderItem={({item}) => (
//                 <Card title={`${item.properties.salary}`}>
//                   <Text>{item.properties.bio}</Text>
//                 </Card>
//                 )
//               }
//             /> */}
//             <JobCard url = {image} 
//                 title={title["title"]} 
//                 headquarters_city={headquarters_city["headquarters_city"]} 
//                 headquarters_country={headquarters_country["headquarters_country"]}
//                 description = {description["description"]}
//                 min={min}
//                 max={max}
//                 total_funding_usd={fnum(total_funding_usd["total_funding_usd"])}
//               />
//           </View> 
//         </ScrollView>       
//       )
//     }

//     // Tab of AboutScreen, nested within the Details Screen
//     function AboutScreen() {
//       return (
//         <ScrollView style={styles.container}>
//           <LinearGradient colors={['lightblue', 'blue', 'teal', 'teal', 'lightblue', 'lightblue']}>
//           <DetailsTitleCard url = {image} 
//             title={title["title"]} 
//             headquarters_city={headquarters_city["headquarters_city"]} 
//             headquarters_country={headquarters_country["headquarters_country"]}
//             min={min}
//             max={max}
//             total_funding_usd={fnum(total_funding_usd["total_funding_usd"])}
//           />

//           <Card containerStyle={styles.cardContainer} title={"About"}>
//             <Text style={styles.description}>{description["description"]}</Text>
//           </Card>

//           <Card>
//               <Text>
//                 Product details, culture
//               </Text>
//           </Card>
//           <Card>
//               <Text>
//                 Other Stuff for Startup to Showcase
//               </Text>
//           </Card>
//           </LinearGradient>
//         </ScrollView>
//       )
//     }

//     return (
//       <MyTabs/>
//     )
//   };

// // Header Card on the Details Page, Does not Display Image if url is null
// function DetailsTitleCard(props) {
//   if (props.url != null) {
//     // Card that is returned if Image URL is not null
//     return (
//       <Card 
//         // title={props.title} titleStyle={styles.title}
//         containerStyle={styles.cardContainer}
//       >
//         <View style={styles.titleContainer}>
//           <Image style={styles.detailsImage} source={{uri: props.url}} resizeMode="center"></Image>
//           <Text style={styles.title}>{props.title}</Text>
//         </View>
//         <View>          
//           <Text style={styles.description}>HQ: {props.headquarters_city}, {props.headquarters_country}</Text>
//         </View>

//         <View style={{flexDirection: 'row'}}>
//           <EmployeesDetail min={props.min} max={props.max}/>
//           <FundingDetail value={props.total_funding_usd}/>
//         </View>
//       </Card>
//       )
//       // Card that is returned if Image URL is null
//   } else {
//     return (
//       <Card 
//         containerStyle={styles.cardContainer}
//       >
//         <View style={styles.titleContainer}>
//           <Text style={styles.title}>{props.title}</Text>
//         </View>
//         <View>          
//           <Text style={styles.description}>HQ: {props.headquarters_city}, {props.headquarters_country}</Text>
//         </View>
//         <View style={{flexDirection: 'row'}}>
//           <EmployeesDetail min={props.min} max={props.max}/>
//           <FundingDetail value={props.total_funding_usd}/>
//         </View>
//       </Card>
//     )
//   }
// }

// function JobCard(props) {
//   return(
//   <Card containerStyle={styles.jobCardContainer}>
//         <View style={styles.titleContainer}>
//           <Image style={styles.detailsImage} source={{uri: props.url}} resizeMode="center"></Image>
//           <View>
//             <Text style={styles.title}>{props.title}</Text>
//             <Text style = {{fontSize: 16, padding: 10,}}>{props.headquarters_city}</Text>
//           </View>
//         </View>
//         {/* <View>          
//           <Text style={styles.description}> {props.headquarters_city}, {props.headquarters_country}</Text>
//         </View> */}

//         <View style = {{alignItems: "center", justifyContent: "center", marginBottom: 4, marginTop: 2,}}>
//           <Text style = {{fontSize: 18, color: "blue", }}> Sprint       Remote        $10/h</Text>
//         </View>

//         <View style = {{marginTop: 2}}>
//           <Text>{props.description} </Text>
//         </View>

//         <View style = {{alignContent: "center", justifyContent: "center", flexDirection: "row", marginTop: 3,}}>
//           <View style = {{flex: 0.33, alignItems: 'center',}}>
//             <Chat innerComponentStyle = {styles.icon2} />
//           </View>
//           <View style = {{flex: 0.34, alignItems: 'center',}}>
//             <Star innerComponentStyle = {styles.icon4}/>
//           </View>
//           <View style = {{flex: 0.33, alignItems: 'center',}}>
//             <Button color = "gray" title = "Apply"/>
//           </View>
//         </View>
//       </Card>
//   )
// }


//   // min and max number of employees, do not display if null
// function EmployeesDetail(props) {
//   if (props.min != null && props.max != null) {
//     return (
//       <Text style={{flex:1, justifyContent: 'center'}}> Employees: {props.min}-{props.max}</Text>
//     )
//   } return (
//     <Text style={{flex:1, justifyContent: 'center'}}> Employees: NA </Text>
//   )
// };

// // Do not display numbers or data on details screen if null (General Component)
// function Detail(props) {
//   if (props.value != null) {
//     return (
//       <Text>{props.value} </Text>
//     )
//   } return null;
// };

// // Do not display funding numbers if null
// function FundingDetail(props) {
//   if (props.value != null) {
//     return (
//       <Text style={{flex:1, justifyContent: 'center'}}>Total Funding: ${props.value} </Text>
//     )
//   } else{
//     return (
//       <Text style={{flex:1}}>Total Funding: NA </Text>
//     )
//   };
// };

// function fnum(x) {
//   if(isNaN(x)) return x;

//   if(x < 9999) {
//       return x;
//   }
//   if(x < 1000000) {
//       return Math.round(x/1000) + "K";
//   }
//   if( x < 10000000) {
//       return (x/1000000).toFixed(2) + "M";
//   }
//   if(x < 1000000000) {
//       return Math.round((x/1000000)) + "M";
//   }
//   if(x < 1000000000000) {
//       return Math.round((x/1000000000)) + "B";
//   }
//   return "1T+";
// };

// const styles = StyleSheet.create({
//   title: {
//     fontSize: 32,
//     borderRadius: 40,
//     flex: 1,
//     padding: 10,
//   },
//   titleContainer: {
//     flexDirection: 'row', 
//     flex: 2,
//   },
//   container: {
//     flex: 1,
//     backgroundColor: 'lightblue',
//   },
//   descriptionTitle:{
//     fontSize: 24,
//     fontWeight: "bold",
//     marginBottom: 10,
//   },
//   description: {
//     fontSize: 16,
//     margin: 5,
//     textAlign: 'center',
//   },
//   detailsImage: {
//     flexDirection: 'row', 
//     flex: 1,
//     padding: 10,
//     borderRadius: 10000,
//   },
//   cardContainer: {
//     borderRadius: 15,
//     backgroundColor: "white",
//   },
//   jobContainer: {

//   },
//   topContainer: {
//     top: 0,
//     flexDirection: "row",
//     justifyContent: "space-around",
//     padding: 8,
//   },
//   seedStagesContainer: {
//     width: screenWidth/4,
//     alignItems: 'center',
//   },
//   chatContainer: {
//     width: screenWidth/4,
//     alignItems: 'center',
//   },
//   checkContainer: {
//     width: screenWidth/4,
//     alignItems: 'center',
//   },
//   starContainer: {
//     width: screenWidth/4,
//     alignItems: 'center',
//   },
//   icon1: {
//     fontSize: 30,
//     color: "#000",
//     opacity: 0.5,
//   },
//   icon2: {
//     fontSize: 30,
//     color: "#000",
//     opacity: 0.5,
//   },
//   icon3: {
//     fontSize: 30,
//     color: "#000",
//     opacity: 0.5,
//   },
//   icon4: {
//     fontSize: 30,
//     color: "gold",
//     // opacity: 100,
//   },
//   jobCardContainer: {
//     borderWidth: 2,
//     borderColor: "gray",
//     borderRadius: 15,
//   }
// })