import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CompanyList from './CompanyList'
import DetailsScreen from './DetailsScreen'

const Stack = createStackNavigator();


// Internal Nested Stack Navigator to navigate from CompanyList Screen to DetailsScreen
export default function DetailsScreenNavigator() {
    return (
        <NavigationContainer independent={true}>
            <Stack.Navigator initialRouteName="CompanyList">
                <Stack.Screen name="CompanyList" component={CompanyList} />
                <Stack.Screen name="Details" component={DetailsScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}