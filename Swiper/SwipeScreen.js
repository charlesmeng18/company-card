import React, { Component } from 'react'
import {connect} from 'react-redux'
import {Text, View, ImageBackground} from 'react-native'
import CardStack, { Card } from 'react-native-card-stack-swiper';
import styles from './styles';
//import Companies from '../server/tester.json'
import CompanySwipeCard from './CompanySwipeCard'
import { Loading } from '../LoadingComponent';
import {useNavigation, NavigationContainer } from '@react-navigation/native';

// Wrap and export
export default function(props) {
  const navigation = useNavigation();

  return <SwipeScreen {...props} navigation={navigation} />;
}

@connect(
  state => ({
    companies: state.companies,
    loading: state.loading,
  }),
  dispatch => ({
    refresh: () => dispatch({type: 'GET_COMPANIES_DATA'}),
  }),
)
class SwipeScreen extends Component {

    renderItem(item, index)  {
        return (
                <Card key={index}>
                    <CompanySwipeCard
                        image={item.profile_image_url}
                        name={item.name}
                        description={item.description}
                        matches={item.match}
                        actions
                        onPressLeft={() => this.swiper.swipeLeft()}
                        onPressRight={() => this.swiper.swipeRight()}
                    />
                </Card>
        )
    }


    render() {

      const { companies, loading, refresh } = this.props;
      const { navigation } = this.props;

        return (
            <ImageBackground style={styles.bg}>
                <View style={styles.cardMargin}>
                    {/* {Companies.map((item) => this.renderItem(item)
                    )} */}

                    <CardStack
                        // Bug: Somewhere here we are unable to read every company
                        loop={true}
                        verticalSwipe={false}
                        renderNoMoreCards={() => null}
                        ref={swiper => this.swiper = swiper}
                    >
                        {/* Temporary Companies Tester.json, in exact same format as API/companies,
                        unclear why this is not working */}
                        {companies.map((item, index) => this.renderItem(item, index))}
                    </CardStack>
                </View>
            </ImageBackground>

        )

    }


    // render() {
    //     return (
    //         <ImageBackground style={styles.bg}>
    //             <View style={styles.containerHome}>
    //                 <CardStack
    //                     loop={true}
    //                     verticalSwipe={false}
    //                     renderNoMoreCards={() => null}
    //                     ref={swiper => this.swiper = swiper}
    //                 >
    //                     {Companies.map((item, index) => (
    //                         <View>
    //                         <Card key={index}>
    //                             <CompanySwipeCard
    //                                 image={item.profile_image_url}
    //                                 name={item.name}
    //                                 description={item.description}
    //                                 matches={item.match}
    //                                 actions
    //                                 onPressLeft={() => this.swiper.swipeLeft()}
    //                                 onPressRight={() => this.swiper.swipeRight()}
    //                             />
    //                         </Card>
    //                         </View>
    //                     ))}
    //                 </CardStack>
    //             </View>
    //         </ImageBackground>
    //     )
    // }

}
