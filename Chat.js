import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';
import Fire from './Fire' // Import our Firebase functions


class Chat extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: (navigation.state.params || {}).name || 'Chat!',
    });

        state = {
        messages: [],
        };

    // Start looking for messages when component is added to the screen
    componentDidMount() {
        // Cal Fire.shared.on method to pass in a callback 
        Fire.shared.on(message =>
            this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, message),
            }))
        );
    }
    
    // Unsubscribe from database on component leaving screen
    componentWillUnmount() {
        Fire.shared.off();
      
    }

    // Knows which side of screen to put messages on
    get user() {
        // Return our name and our UID for GiftedChat to parse
        return {
          name: this.props.navigation.state.params.name,
          _id: Fire.shared.uid,
        };
      }

    render() {
        return (
            <GiftedChat messages={this.state.messages}
            onSend={Fire.shared.send}
            user={this.user} />
        )
    }
    }
const styles = StyleSheet.create({});
export default Chat;