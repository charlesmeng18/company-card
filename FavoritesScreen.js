import React, {Component} from 'react'
import { View, Text, Card, SafeAreaView, TouchableOpacity, FlatList} from 'react-native';
import styles from './styles.js'
import Loading from './LoadingComponent'

const FAV = 'http://localhost:3001/favorites';
// const FAV = 'http://192.168.1.91:3001/favorites'; // TODO: change this IP address to your own internal IP address

export default class FavoritesScreen extends Component {
    constructor (props) {
        super(props);

        this.state={
            favorites:[]
        }
    }

    // Grab Favorited Companies with GET Request Initially
    componentDidMount = () => {
        fetch(FAV, {
            method: 'GET'
         })
         .then((response) => response.json())
         .then((responseJson) => {
            //console.log(responseJson);
            this.setState({
               favorites: responseJson
            })
         })

     }

     // On any changes in the Favorites, updates the favorites State to Display
     // New Favorited Companies
     componentDidUpdate = () => {
        fetch(FAV, {
            method: 'GET'
         })
         .then((response) => response.json())
         .then((responseJson) => {
            //console.log(responseJson);
            this.setState({
               favorites: responseJson
            })
         })
     }

     // Temporary FlatList to display favorites, needs styling still
     // Successfully updates on additions to the server
     render() {
        if (this.state.favorites.length > 0) {
         return (
            <SafeAreaView style={styles.container}>
                <FlatList
                data ={this.state.favorites}
                renderItem={({item}) => (
                        <TouchableOpacity>
                            <View style={styles.item}>
                                <Text>{item.name}</Text>
                            </View>
                        </TouchableOpacity>

                )}>
                </FlatList>
            </SafeAreaView>
            )
        } else {
            return (
            <SafeAreaView style={styles.container}>
              <Text>Loading</Text>
            </SafeAreaView>
            );
        }
     }

}
