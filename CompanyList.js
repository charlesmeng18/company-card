import React, {Component} from 'react';
import {Image, FlatList, SafeAreaView, StyleSheet, Text, TextInput, View, TouchableOpacity } from 'react-native';
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import Constants from 'expo-constants';
import {useNavigation, NavigationContainer } from '@react-navigation/native';
import Home from './components/homeButton';
import Chat from './components/chatButton';
import Bookmark from './components/bookMarkButton';
import Alert from './components/alertButton';
import Account from './components/accountButton';
import {screenWidth, screenHeight} from './constants/dimensions';
import { connect } from 'react-redux';
import { Loading } from './LoadingComponent';
import styles from './styles'

// Wrap and export
export default function(props) {
  const navigation = useNavigation();

  return <CompanyList {...props} navigation={navigation} />;
}

@connect(
  state => ({
    companies: state.companies,
    loading: state.loading,
  }),
  dispatch => ({
    refresh: () => dispatch({type: 'GET_COMPANIES_DATA'}),
  }),
)
class CompanyList extends Component {

    render() {

        const { companies, loading, refresh } = this.props;
        const { navigation } = this.props;

        //console.log(JSON.stringify(companies, null, 2));

        if (loading) {
          return(
            <SafeAreaView style={styles.container}>
              <Loading />
            </SafeAreaView>
          );
        } else {

           return(
              <SafeAreaView style={styles.container}>

              <FlatList
                  data={companies}
                  renderItem={({item}) => (

                    <Item
                      title={item["name"]}
                      headquarters_city ={item["city"]}
                      headquarters_country={item["country"]}
                      url={item["homepage_url"]}
                      image_url={item[ "profile_image_url"]}
                      description={item["description"]}
                      min_employees={item["num_employees_min"]}
                      max_employees={item["num_employees_max"]}
                      total_funding_usd={item["total_funding_usd"]}
                      navigation = {navigation}
                      //founders_array = {item["data"]["items"][1]["relationships"]["founders"]["items"]}
                    />
                  )}
                  >
                </FlatList>
              </SafeAreaView>
            );

          } // if/else loading

      } // render

  } // company list class



function fnum(x) {
  if (isNaN(x)) return x;

  if (x < 9999) {
    return x;
  }
  if (x < 1000000) {
    return Math.round(x / 1000) + "K";
  }
  if (x < 10000000) {
    return (x / 1000000).toFixed(2) + "M";
  }
  if (x < 1000000000) {
    return Math.round((x / 1000000)) + "M";
  }
  if (x < 1000000000000) {
    return Math.round((x / 1000000000)) + "B";
  }
  return "1T+";
};

function Item({title, founders_array, headquarters_city, headquarters_country, url, image_url, description, min_employees, max_employees, total_funding_usd, navigation}) {
  //console.log(image_url);

  // getting uri for image
  if (image_url !== null || image_url === "") {
    var useImage = image_url;
  } else {
    useImage = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQMAAADCCAMAAAB6zFdcAAAAY1BMVEXDw8MAAADIyMhwcHCKioqkpKS8vLyurq7JycmAgICQkJCfn594eHhra2uqqqp7e3tZWVlUVFRCQkK2trZISEi+vr4XFxdNTU0oKCghISEzMzNlZWWYmJgLCws9PT3Pz88eHh57cn8hAAACOUlEQVR4nO3b6W6CQBhGYWa0Oopr0Wpdau//KlsUWXTGBmhifDnP3xqSOYAfII0iAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAjrHtPXsJrY16bY2evYSW7Ni0N37tI8H2/6FB//UbxIM2YokGb22WYN80GrTagFQD65pMOaUGbjhZTGNXfwM6Ddz8/A2/qb8BmQb5ZcKy7pEg1CCf9bO6G5BpMMsbTGquR6aBjfMG0642iJI/r3tDc1OngXu/Nlj7P+fieeJdqU6D/EAI3AK6afo338wQamCT1e8qt4GLpGx0zjxrFWrwu6uTWRI66bOj5NO3AaUGjz60y86Uxf1h0pEG5y+DbGrcRehGA3soPTIa3i63Gw0G5cdm2042KC4dzvY3Z0MXGrjJzQPUQ3XBmg2sLe3qdIk3BtVPKzZw8+2yuCxeH+8a7Cpng2ID1yvva7e5S2BMrxxBsEF2F/11WZUbeRIYE5fWLNjgOggn6b62Q2+Cys2lXoNiEKb72n4EGqyKs0GuQWkQHtfRaRlIYMw8j6DWoDIIV6dHP8fmv8+pNVhXjv3egwTmI9+AVgPvIAy5/hCh1SAwCEOy22ipBsFBGHK5jZZqEH3XbHB5sqbUwC1qJsierAk1aPRuVvogXqhB8veKPRKlBnbbqMHO6jQ47RslMGbqVBq45i9qjp1IAztsTqQB72g2Pg0Kr96Ad7Yj3t1P8T8cAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAtf0A7N8ZC2Gr650AAAAASUVORK5CYII=';
  }

  // Edits funding in presentable format of USD
  // let funding = '';
  // if (total_funding_usd > 1000000 && total_funding_usd < 1000000000) {
  //   total_funding_usd = Math.floor(total_funding_usd / 100000);
  //   funding = "$" + total_funding_usd + " m";
  // } else if (total_funding_usd > 1000) {
  //     total_funding_usd = Math.floor(total_funding_usd % 1000);
  //     funding = "$" + total_funding_usd + " k";
  // } else {
  //   funding = "$" + Math.floor(total_funding_usd);
  // }
  let funding = fnum(total_funding_usd)

  // Displayes employees if data is available
  let size;
  if (min_employees === null || max_employees === null) {
    size = "Unknown";
  } else {
    size = min_employees + " - " + max_employees;
  }

  return (
      <View style={styles.item}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("Details", {
              title: {title},
              headquarters_city: {headquarters_city},
              headquarters_country: {headquarters_country},
              description: {description},
              url: {url},
              image_url: {image_url},
              min_employees: {min_employees},
              max_employees: {max_employees},
              total_funding_usd: {total_funding_usd},
              founders_array: {founders_array},
            })}>

            {/* part container for each card
            contains company logo, name, location,
            position available, and description */}
            <View style = {styles.box}>
                {/* company logo */}
                <View style = {styles.logoContainer}>
                  <Image
                      style={styles.companyLogo}
                      source={{uri: useImage}}
                      resizeMode = "center"
                  />
                </View>

                {/* company title, location and role offered */}
                <View style={styles.overviewContainer}>
                  {/* 10 chars wide */}
                  <Text numberOfLines={2} style={styles.title}>
                    {title}
                  </Text>
                  {/* 14 chars wide */}
                  <Text style = {styles.location}>
                    {headquarters_city}
                  </Text>
                  <Text style = {styles.position}>
                    Position available
                  </Text>
                </View>

                {/* visuals of company */}
                <View style = { styles.visualsContainer}>
                  <Text style = {styles.funding}>
                    {/* Funding {fnum(total_funding_usd)} */}
                    {funding}
                  </Text>

                  <Image
                    source={require("./assets/graph.png")}
                    style = {styles.graph} />

                  <View style = {styles.companySizeContainer}>
                    <MaterialCommunityIconsIcon
                      name="account"
                      style={styles.accountIcon}>
                    </MaterialCommunityIconsIcon>
                    <Text style = {styles.employeeCount}>
                      {size}
                    </Text>
                  </View>

                  <Text style = {styles.salary}>
                    Salary
                  </Text>
                </View>
            </View>

            {/* description of company */}
            <View style = {styles.detialsContainer}>
              {/* 108 chars long */}
              <Text style={styles.description}>
                {description}
              </Text>
            </View>
        </TouchableOpacity>
      </View>
    )
  }
