# Run this file like: ./runs.sh
# Make sure that it is executable: chmod +x run.sh

json-server --watch db.json --static public -d 2000 -p 3001 --host 0.0.0.0
