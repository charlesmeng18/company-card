import React, {Component} from 'react';
import {Stylesheet, TouchableOpacity, Text, View} from 'react-native';

export default class CompanyButton extends Component {
    constructor(props) {
        super(props)
        this.handleClick = this.handleClick.bind(this);
        this.state = {displayDetails: false};
    }
    handleClick(){
        const newState = !this.state.displayDetails;
        this.setState({displayDetails:newState});
    }
    render() {
        const displayDetails = this.state.displayDetails;
        return (
            <TouchableOpacity
                onPress={() => this.handleClick()}>
                    <CompanyDisplay displayDetails ={this.state.displayDetails} />
            </TouchableOpacity>
        )
    }
}


function CompanyDisplay(displayDetails) {
    if (displayDetails) {
        return (
            <View>
                <Text>"Hi"</Text>
            </View>
        )
    return (
        <View>
            <Text>"More detailed card"</Text>
        </View>
    )
    }
}