import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import { Tile } from 'react-native-elements';
import { connect } from 'react-redux';
import { Loading } from './LoadingComponent';

const mapStateToProps = state => {
    return {
      jobs: state.jobs
    }
  }


class Menu extends Component {

  static navigationOptions = {
    title: 'Menu'
  }

  render() {

    const renderMenuItem = ({item, index}) => {
      return(
        <Tile
          key={index}
          title={item.title}
          caption={item.description}
          featured
          onPress={() => navigate('Jobs', { userId: item.id })}
        />

      );
    }

    const { navigate } = this.props.navigation;

    if (this.props.jobs.isLoading) {
      return(
        <Loading />
      );
    } else if (this.props.jobs.errMess) {
      return(
        <View>
          <Text>{this.props.jobs.errMess}</Text>
        </View>
      );
    } else {
      return(
        <FlatList
          data={this.props.jobs.jobs}
          renderItem={renderMenuItem}
          keyExtractor={item => item.id.toString()}
        />
      );
    }


  }

}

export default connect(mapStateToProps)(Menu);
