import React, { Component } from 'react';

import Home from './HomeComponent';
import Jobs from './JobsComponent';

import Menu from './MenuComponent';
import { View, Platform, Image, StyleSheet, ScrollView, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { fetchCompanies } from './redux/ActionCreators';
import { fetchJobs } from './redux/ActionCreators';

const mapStateToProps = state => {
  return {
    companies: state.companies,
    jobs: state.jobs,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchCompanies: () => dispatch(fetchCompanies()),
  fetchJobs: () => dispatch(fetchJobs()),
})


const HomeNavigator = createStackNavigator({
    Home: { screen: Home }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#FFBE50"
      },
      headerTitleStyle: {
          color: "#fff"
      },
      headerTintColor: "#fff",
      headerLeft: <Icon name='menu'
                        size={24}
                        color='white'
                        onPress={ () => navigation.toggleDrawer() } />
    })
});

const JobsNavigator = createStackNavigator({
    Jobs: { screen: Jobs }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#FFBE50"
      },
      headerTitleStyle: {
          color: "#fff"
      },
      headerTintColor: "#fff",
      headerLeft: <Icon name='menu'
                        size={24}
                        color='white'
                        onPress={ () => navigation.toggleDrawer() } />
    })
});




class Main extends Component {

  componentDidMount() {
    this.props.fetchCompanies();
    this.props.fetchJobs();
  }


  render() {
    return(
      <View style={{ flex: 1, paddingTop: Platform.OS === 'ios' ? 0 : Expo.Constants.statusBarHeight }}>
        <MainNavigator />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Main);
