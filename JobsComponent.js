import React, { Component } from 'react';
import { View, ScrollView, Text, Switch, Image, StyleSheet } from 'react-native';
import { Card, Icon } from 'react-native-elements';


function RenderItem(props) {
  const item = props.item;

  const renderHeaderButtons = () => {
      return (
        <View style={{flexDirection: 'row', justifyContent: 'center',}}>
          <Icon
              raised
              reverse
              name='times'
              type='font-awesome'
              color='#f00'
           />
           <Icon
               raised
               reverse
               containerStyle={{float: 'right'}}
               name='heart'
               type='font-awesome'
               color='#0f0'
            />
          </View>
      );
  };

  if (props.isLoading) {
    return(
      <Loading />
    );
  } else if (props.errMess) {
    return(
      <View>
        <Text>{props.errMess}</Text>
      </View>
    );
  } else {
    if (item != null) {
      return (
        <Card
          title={
            <View style={{
              margin: 20,
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
              <Icon
                  name='user'
                  type='font-awesome'
                  color='#666'
               />
               <Switch
                  ios_backgroundColor='#ff0'
                />
               <Icon
                   name='comments'
                   type='font-awesome'
                   color='#666'
                />
              </View>
          }
          >
          <Text style={{
            fontSize: 18,
            fontWeight: 'bold',
            margin: 10
          }}>
            {item.title}, {item.company}
          </Text>
          <Text style={{margin: 10}}>
            {item.description}
          </Text>
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly'
          }}>
            <Icon
                raised
                reverse
                name='times'
                type='font-awesome'
                color='#f00'
             />
             <Icon
                 raised
                 reverse
                 name='heart'
                 type='font-awesome'
                 color='#0f0'
              />
            </View>
        </Card>
      );
    } else {
      return (<View></View>);
    }
  }


}

class Jobs extends Component {

    static navigationOptions = {
        title: 'Jobs'
    };

    render() {
        return(
          <ScrollView>
            <RenderItem
              item={this.props.jobs.jobs[0]}
              isLoading={this.props.jobs.isLoading}
              errMess={this.props.jobs.errMess}
            />
          </ScrollView>
        );
    }
}


export default Jobs;
