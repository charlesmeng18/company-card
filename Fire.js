import firebase from 'firebase'; 

// Our firebase functions
class Fire {
  constructor() {
    this.init();

    this.observeAuth();
  }

  // 2.
    init = () =>
        firebase.initializeApp({
            apiKey: "AIzaSyBWRwyCHKtnDAjav1yHjMym0in6vT-3GIw",
            authDomain: "seedstages-5042c.firebaseapp.com",
            databaseURL: "https://seedstages-5042c.firebaseio.com",
            projectId: "seedstages-5042c",
            storageBucket: "seedstages-5042c.appspot.com",
            messagingSenderId: "963612042179",
            appId: "1:963612042179:web:bd98bd5261bdfbb6239376",
            measurementId: "G-LN9LE7QLXQ"
        });

    observeAuth = () =>
        firebase.auth().onAuthStateChanged(this.onAuthStateChanged);

    onAuthStateChanged = user => {
        if (!user) {
            try {
            // 4.
                firebase.auth().signInAnonymously();
            } catch ({ message }) {
                alert(message);
            }
        }
        };

    get uid() {
        return (firebase.auth().currentUser || {}).uid;
    }

    get timestamp() {
        return firebase.database.ServerValue.TIMESTAMP;
      }

    append = message => this.ref.push(message);

    send = messages => {
    for (let i = 0; i < messages.length; i++) {
        const { text, user } = messages[i];
        // 4.
        const message = {
            text,
            user,
            timestamp: this.timestamp,
        };
        this.append(message);
    }
      };

    get ref() {
        return firebase.database().ref('messages');
    }

    on = callback =>
        this.ref
        .limitToLast(20)
        .on('child_added', snapshot => callback(this.parse(snapshot)));

    parse = snapshot => {
        const { timestamp: numberStamp, text, user } = snapshot.val();
        const { key: _id } = snapshot;
        // 2.
        const timestamp = new Date(numberStamp);
        // 3.
        const message = {
          _id,
          timestamp,
          text,
          user,
        };
       return message;
    }

    off() {
        this.ref.off();
      
    }
}